import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ToastController } from '@ionic/angular';

import { TranslateService } from '@ngx-translate/core';

import { NavController } from '@ionic/angular';

//import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';

import { FcmService } from '../service/fcm.service';
import { ToastService } from '../service/toast.service';

import {Firebase} from '@ionic-native/firebase/ngx';
import {AlertController} from '@ionic/angular';
import * as firebase from 'firebase';

import { FCM } from '@ionic-native/fcm/ngx';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private router: Router,
        public translate: TranslateService,
        public navigation: NavController,
        private auth: AngularFireAuth,
        private storage: Storage,
        // private fcm: FcmService,
        private toastr: ToastService,
        public toastController: ToastController,


        private fcm: FCM,
        private firebase: Firebase,
        private alertCtrl: AlertController,
    ) {
        // Verifica se existe na lista de linguagens
        let language: string;
        if(environment.languages.indexOf(navigator.language.split('-')[0]) != -1)
            language = navigator.language.split('-')[0];
        else
            language = environment.defaultLanguage;

        translate.setDefaultLang(language);

        this.initializeApp();
    }

    public async initializeApp() {   
        this.platform.ready().then(async () => {
            this.statusBar.styleDefault();
            this.statusBar.backgroundColorByHexString('#00346E');
            this.statusBar.styleLightContent();

            const dataUser = await this.storage.get('user');
            if(dataUser && dataUser.idioma) {
                this.translate.setDefaultLang(dataUser.idioma);
            }

            this.auth.auth.onAuthStateChanged(user => {
                this.splashScreen.hide();
                 // subscribe to a topic
                // this.fcm.subscribeToTopic('Deals');

                // get FCM token
                this.fcm.getToken().then(token => {
                    console.log(token);
                });

                // ionic push notification example
                this.fcm.onNotification().subscribe(data => {
                    console.log(data);
                    if (data.wasTapped) {
                    console.log('Received in background');
                    } else {
                    console.log('Received in foreground');
                    }
                });      

                // refresh the FCM token
                this.fcm.onTokenRefresh().subscribe(token => {
                    console.log(token);
                });

                // unsubscribe from a topic
                // this.fcm.unsubscribeFromTopic('offers');
                if(user && this.router.url === '/start') this.navigation.navigateForward('/pages/menu/home', {animated: true});
            });
        });
    }
}
