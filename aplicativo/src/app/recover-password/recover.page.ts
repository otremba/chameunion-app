import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'page-recover',
	templateUrl: 'recover.page.html',
	styleUrls: ['../login/login.page.scss'],
})
export class RecoverPage {

	public email: string;
	public recoverError: string;
	public send: boolean = false;

	constructor(
		public firebaseAuth: AngularFireAuth,
		public loading: LoadingController,
		public translate: TranslateService
	) {
		//
	}

	public async recover(form) {
		this.recoverError = null;

		if(form.valid) {
			this.loading.create({message: this.translate.instant('wait') + '...'}).then(loader => {
				loader.present();

				try {
					this.firebaseAuth.auth.sendPasswordResetEmail(this.email).then(_response => {
						this.send = true;
						this.loading.dismiss();
					})
					.catch(_error => {
						this.recoverError = 'invalid_email';
						this.loading.dismiss();
					});
				}
				catch(e) {
					this.recoverError = 'invalid_email';
					this.loading.dismiss();
				}
			})

		}
		else {
			this.recoverError = 'invalid_email';
		}
	}
}
