import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

//Ionic
import { NavController, PopoverController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';
import * as moment from 'moment';

//Firebase
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';

import { TranslateService } from '@ngx-translate/core';

import { FileChooser } from '@ionic-native/file-chooser/ngx'
// import { File } from '@ionic-native/file'
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';


@Component({
  selector: 'app-innovations-popover',
  templateUrl: './innovations-popover.component.html',
  styleUrls: ['./innovations-popover.component.scss'],
})
export class InnovationsPopoverComponent implements OnInit {
  //var formulario reativo
	public innovationForm: FormGroup;
	//submit do form
	public submitted: boolean = false;

	public products: any;
  	public allProducts: any;
  	public key: any;
	public userId: any;
	public download: any;
	public progressUpload: any;
	public uploadTask: any;

  constructor(
    public formbuilder: FormBuilder,
		public afAuth: AngularFireAuth,
		public alertController: AlertController,
		public loading: LoadingController,
		public translate: TranslateService,
		public popoverController: PopoverController,
		public toastController: ToastController,
		private fileChooser: FileChooser,
		private filePath: FilePath,
		private file: File,

  ) { 
	

    this.innovationForm = this.formbuilder.group({
			id: null,
			titulo: [null, [Validators.required]],
			descricao: [null, [Validators.required]],
			arquivo: [null, [Validators.required]]
		})
  }

  ngOnInit() {
	this.start();
  }

  async start(){
	firebase.auth().onAuthStateChanged((user) => {
		if (user) {
		  this.userId = user.uid;
		  console.log(this.progressUpload);
		} else {
		}
	  });
	}

  async submitInnovation() {

	this.submitted = true;
		let usuario: any = Object.assign({}, this.innovationForm.value);
		usuario.id;
		usuario.titulo;
		usuario.descricao;
		usuario.arquivo;
		usuario.status = 'Em análise'; 

		if (usuario.titulo == null) {			
			const toast = await this.toastController.create({
				message: 'Preencha todos os campos para continuar!',
				duration: 2000
			  });
			  toast.present();
		}else{
			let date = moment().format('DD/MM/YYYY h:mm:ss');
			this.key = firebase.database().ref('/app/open-innovation/').push({id: '0', title: usuario.titulo, desc: usuario.descricao, archive: usuario.arquivo, status: usuario.status, date: date });
			this.popoverController.dismiss();

			const user: any = await AppGlobal.getInstance().getUser();
			firebase.database().ref('app/open-innovation').once('value').then(snapshot => {
				let idInnovation = this.key.key;				
				firebase.database().ref('app/open-innovation/' + idInnovation).update({id: idInnovation, user: user.id, archive: this.download});
				location.reload(true);
			});
			const toast = await this.toastController.create({
				message: 'Projeto criado com sucesso!',
				duration: 2000
			  });
			toast.present();
		}
	}
	choose() {
		this.fileChooser.open().then((uri) => {	
		  this.filePath.resolveNativePath(uri).then(filePath => {
			let dirPathSegments = filePath.split('/');
			let fileName = dirPathSegments[dirPathSegments.length-1];
			dirPathSegments.pop();
			let dirPath = dirPathSegments.join('/');
			this.file.readAsArrayBuffer(dirPath, fileName).then(async (buffer) => {
			  await this.upload(buffer, fileName);
			}).catch((err) => {
			  alert(err.toString());
			});
		  });
		});
	}
	async upload(buffer, name){
		let blob = new Blob([buffer], {type: "application/pdf"});

		let storageRef = firebase.storage().ref('innovation-documents/' + name);

		this.uploadTask = storageRef.put(blob).on('state_changed', (snapshot: firebase.storage.UploadTaskSnapshot) => {
			this.progressUpload = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
			console.log('Upload is ' + this.progressUpload + '% done');
			storageRef.getDownloadURL().then(downloadURL => {
				console.log(downloadURL);
				this.download = downloadURL;
			});
		});
	}
}
