import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ThemeModule } from 'src/theme/theme.module';


import { IonicModule } from '@ionic/angular';

import { InteressePage } from './interesse.page';

const routes: Routes = [
  {
    path: '',
    component: InteressePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ThemeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InteressePage]
})
export class InteressePageModule {}
