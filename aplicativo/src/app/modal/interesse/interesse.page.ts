import { Component, OnInit } from "@angular/core";
import * as firebase from "firebase";
import AppGlobal from "src/app/app.global";
import { ModalController } from '@ionic/angular';


@Component({
  selector: "app-interesse",
  templateUrl: "./interesse.page.html",
  styleUrls: ["./interesse.page.scss"],
})
export class InteressePage implements OnInit {
  public interesses: any;
  public AllInteresses: any;
  public highlights: any;
  public status: boolean = false;
  public status2: boolean = false;
  public status3: boolean = false;
  public status4: boolean = false;
  public status5: boolean = false;
  public status6: boolean = false;
  public status7: boolean = false;
  public status8: boolean = false;
  public status9: boolean = false;
  public status10: boolean = false;
  public status11: boolean = false;
  public status12: boolean = false;
  public status13: boolean = false;
  public status14: boolean = false;
  public status15: boolean = false;
  public status16: boolean = false;
  public status17: boolean = false;
  public wtf: any;

  constructor(
		private modalController: ModalController,
  ) {}

  ngOnInit() {
    this.start();
  }

  async start() {
    const user: any = await AppGlobal.getInstance().getUser();

    firebase.database().ref("app/usuarios/" + user.id + "/interesses").once('value').then(snapshot => {
      let item = snapshot.val();
      this.highlights = Object.values(item);
      this.status = this.highlights[0].status;
      this.status2 = this.highlights[1].status;
      this.status3 = this.highlights[2].status;
      this.status4 = this.highlights[3].status;
      this.status5 = this.highlights[4].status;
      this.status6 = this.highlights[5].status;
      this.status7 = this.highlights[6].status;
      this.status8 = this.highlights[7].status;
      this.status9 = this.highlights[8].status;
      this.status10 = this.highlights[9].status;
      this.status11 = this.highlights[10].status;
      this.status12 = this.highlights[11].status;
      this.status13 = this.highlights[12].status;
      this.status14 = this.highlights[13].status;
      this.status15 = this.highlights[14].status;
      this.status16 = this.highlights[15].status;
      this.status17 = this.highlights[16].status;

      this.AllInteresses = [
        { nome: "Análises", status: this.status },
        { nome: "Artigos científicos", status: this.status2 },
        { nome: "Ativos", status: this.status3 },
        { nome: "Claims", status: this.status4 },
        { nome: "Cosméticos", status: this.status5 },
        { nome: "Dados de mercado", status: this.status6 },
        { nome: "Hair care", status: this.status7 },
        { nome: "Home care", status: this.status8 },
        { nome: "Ingredientes", status: this.status9 },
        { nome: "Insumos cosméticos", status: this.status10 },
        { nome: "Insumos farmacêuticos", status: this.status11 },
        { nome: "Lançamentos", status: this.status12 },
        { nome: "Life sciences", status: this.status13 },
        { nome: "Novidades", status: this.status14 },
        { nome: "Personal care", status: this.status15 },
        { nome: "Skin care", status: this.status16 },
        { nome: "Tendências", status: this.status17 },
      ];
      // console.log(this.AllInteresses);

    });
  }

  async addValue(nome) {
    const user: any = await AppGlobal.getInstance().getUser();
    firebase.database().ref('app/usuarios/' + user.id + '/interesses/' + nome ).once('value').then(snapshot => {
			this.wtf = [];
			snapshot.forEach(child => {
        this.wtf.push(child.val());
        // console.log(this.wtf);
        if (this.wtf =='false') {
          firebase.database().ref('app/usuarios/' + user.id + '/interesses/' + nome).update({status: 'true'})
        }else{
          firebase.database().ref('app/usuarios/' + user.id + '/interesses/' + nome).update({status: 'false'})
        }
			});
    });
    
  }
  async closeModal() {
		const onClosedData: string = "Wrapped Up!";
		await this.modalController.dismiss(onClosedData);
	}
} 