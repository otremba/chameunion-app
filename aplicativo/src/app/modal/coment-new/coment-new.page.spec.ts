import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentNewPage } from './coment-new.page';

describe('ComentNewPage', () => {
  let component: ComentNewPage;
  let fixture: ComponentFixture<ComentNewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComentNewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentNewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
