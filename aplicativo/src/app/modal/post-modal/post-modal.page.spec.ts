import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostModal } from './post-modal.page';

describe('PostModal', () => {
  let component: PostModal;
  let fixture: ComponentFixture<PostModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostModal ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
