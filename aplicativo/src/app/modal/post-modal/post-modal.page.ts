import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import * as moment from 'moment';
import * as firebase from 'firebase';
import { NavController, PopoverController } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';

//file
import { FileChooser } from '@ionic-native/file-chooser/ngx'
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
	selector: 'app-post-modal',
	templateUrl: './post-modal.page.html',
	styleUrls: ['./post-modal.page.scss'],
})
export class PostModal {
	public postForm: FormGroup;
	public submitted: boolean = false;
	public key: any;
	public uploadTask: any;
	public progressUpload: any;
	public downloadPDF: any = '0';
	public downloadPNG: any = '0';

	constructor(
		private modalController: ModalController,
		public formbuilder: FormBuilder,
		public toastController: ToastController,
		public popoverController: PopoverController,
		private fileChooser: FileChooser,
		private filePath: FilePath,
		private file: File,

	) {

		this.postForm = this.formbuilder.group({
			id: null,
			publicacao: [null, [Validators.required]],
			arquivo: [null, [Validators.required]]
		})
	}


	ngOnInit() {
		this.start();
	}

	public async start() {

	}

	async submitPub() {

		this.submitted = true;
		let usuario: any = Object.assign({}, this.postForm.value);
		usuario.id;
		usuario.publicacao;
		usuario.arquivo;

			const user: any = await AppGlobal.getInstance().getUser();

			let date = moment().format('DD/MM/YYYY h:mm:ss');
			this.key = firebase.database().ref('/app/feed/').push({ id: '0', user: user.nome, corporation: user.empresa, title: usuario.publicacao, date: date});
			this.popoverController.dismiss();
				firebase.database().ref('app/feed').once('value').then(snapshot => {
					let idPub = this.key.key;
						firebase.database().ref('app/feed/' + idPub).update({ id: idPub, user: user.nome, local: this.downloadPDF, archive: this.downloadPNG });
						// firebase.database().ref('app/feed/' + idPub).child("likes").set({
						// 	id: true 
						// });
	
					// location.reload(true);
				});
			
			const toast = await this.toastController.create({
				message: 'Publicado!',
				duration: 2000
			});
			toast.present();
		
		this.closeModal();
		// location.reload(true);
	}
	chooseJPG() {
		this.fileChooser.open().then((uri) => {
			this.filePath.resolveNativePath(uri).then(filePath => {
				let dirPathSegments = filePath.split('/');
				let fileName = dirPathSegments[dirPathSegments.length - 1];
				dirPathSegments.pop();
				let dirPath = dirPathSegments.join('/');
				this.file.readAsArrayBuffer(dirPath, fileName).then(async (buffer) => {
					await this.uploadJPG(buffer, fileName);
				}).catch((err) => {
					alert(err.toString());
				});
			});
		});
	}
	choosePDF() {
		this.fileChooser.open().then((uri) => {
			this.filePath.resolveNativePath(uri).then(filePath => {
				let dirPathSegments = filePath.split('/');
				let fileName = dirPathSegments[dirPathSegments.length - 1];
				dirPathSegments.pop();
				let dirPath = dirPathSegments.join('/');
				this.file.readAsArrayBuffer(dirPath, fileName).then(async (buffer) => {
					await this.uploadPDF(buffer, fileName);
				}).catch((err) => {
					alert(err.toString());
				});
			});
		});
	}
	async uploadJPG(buffer, name) {
		this.downloadPNG = '0';

		let blob = new Blob([buffer], { type: "application/jpg" });

		let storageRef = firebase.storage().ref('feed-documents/' + name);

		this.uploadTask = storageRef.put(blob).on('state_changed', (snapshot: firebase.storage.UploadTaskSnapshot) => {
			this.progressUpload = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
			console.log('Upload is ' + this.progressUpload + '% done');
			storageRef.getDownloadURL().then(downloadURL => {
				console.log(downloadURL);
				this.downloadPNG = downloadURL;
			});
		});
	}
	async uploadPDF(buffer, name) {
		this.downloadPDF = '0';

		let blob = new Blob([buffer], { type: "application/pdf" });

		let storageRef = firebase.storage().ref('feed-documents/' + name);

		this.uploadTask = storageRef.put(blob).on('state_changed', (snapshot: firebase.storage.UploadTaskSnapshot) => {
			this.progressUpload = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
			console.log('Upload is ' + this.progressUpload + '% done');
			storageRef.getDownloadURL().then(downloadURL => {
				console.log(downloadURL);
				this.downloadPDF = downloadURL;
			});
		});
	}

	

	async closeModal() {
		const onClosedData: string = "Wrapped Up!";
		await this.modalController.dismiss(onClosedData);
	}

}
