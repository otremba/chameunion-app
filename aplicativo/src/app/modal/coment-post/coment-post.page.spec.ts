import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentPostPage } from './coment-post.page';

describe('ComentNewPage', () => {
  let component: ComentPostPage;
  let fixture: ComponentFixture<ComentPostPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComentPostPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentPostPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
