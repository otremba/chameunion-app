import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as firebase from 'firebase';
import AppGlobal from 'src/app/app.global';
import { ActionSheetController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';


import * as moment from 'moment';



@Component({
  selector: 'app-coment-post',
  templateUrl: './coment-post.page.html',
  styleUrls: ['./coment-post.page.scss'],
})
export class ComentPostPage {

  public textSaved: any;
  public commentForm: FormGroup;
  public submitted: boolean = false;
  public key: any;
  public idioma: any;
  public perfilId: any;
  public product: any;
  public url: any;
  public allItems: any;
  public allIds: any;
  public ultimo: any;
  public batata: any;
  public userName: any;
  public commentName: any;
  public idComment: any;

  searchValue: string = '';
  actionSheet: any;


  constructor(private modalController: ModalController, public formbuilder: FormBuilder, public actionSheetController: ActionSheetController, public alertController: AlertController) {

    this.commentForm = this.formbuilder.group({
      comentario: [null, [Validators.required]],
    })
  }


  ngOnInit() {
    this.start();
  }

  public async start() {
    var url_atual = window.location.href;
    this.url = url_atual.split("/");
    console.log(this.url);
    this.key = this.url;
    const user: any = await AppGlobal.getInstance().getUser();
    this.idioma = user.idioma;
    this.userName = user.nome;
    this.perfilId = user.perfil_id;
    firebase.database().ref('app/feed/' + this.key[6] + '/comentarios').once('value').then(snapshot => {
      let item = snapshot.val();
      this.allItems = Object.values(item);
      console.log(this.allItems);

      this.allIds = Object.keys(item);


    });
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }
  // comenta(){
  //   this.textSaved = (<HTMLInputElement>document.getElementById('inputTitle')).value;
  // }

  async submitInnovation() {
    const user: any = await AppGlobal.getInstance().getUser();
    console.log(user);


    this.submitted = true;
    let usuario: any = Object.assign({}, this.commentForm.value);
    usuario.comentario;
    console.log(usuario);


    firebase.database().ref('app/feed/' + this.key[6] + '/' + 'comentarios').once('value').then(snapshot => {
      let idNotice = user.id;
      let nomeUser = user.nome;
      let date = moment().format('YYYY-MM-DD HH:mm:ss');
      this.batata = firebase.database().ref('app/feed/' + this.key[6] + '/' + 'comentarios').push({ idComment: '0', status: '1', id: idNotice, nome: nomeUser, data: date, comentario: usuario.comentario });

      firebase.database().ref('app/feed/' + this.key[6] + '/comentarios').once('value').then(snapshot => {

        this.ultimo = this.batata.key;
        firebase.database().ref('app/feed/' + this.key[6] + '/comentarios/' + this.ultimo).update({ idComment: this.ultimo });
        this.back();
        let item = snapshot.val();
        this.allItems = Object.values(item);

        // console.log(item);

        let date = moment().format('YYYY-MM-DD HH:mm:ss');
        firebase.database().ref('/app/logs/' + user.id + '/comentario' + '/' + this.key[6]).push().set(date);

      });
    });
    this.searchValue = null;
  }
  public back(){
    window.history.back();
  }
  public setDate(date): Date {
    return moment(date).toDate();
  }
  presentActionSheet(nome, id) {
    console.log(id);
    this.idComment = id;

    this.actionSheet = this.actionSheetController.create({
      header: 'Options',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          if (this.userName == nome) {
            // var firstDivID = document.getElementsByTagName('ion-card')[0].id;
            // console.log(firstDivID);


            this.presentConfirm(this.idComment);
          } else {
            this.erroDelete();
          }
          console.log(this.idComment);
          console.log(nome);
        }
      }]
    }).then(actionsheet => {
      actionsheet.present();
    });
  }
  async presentConfirm(id) {
    const alert = await this.alertController.create({
        header: 'Aviso',
        message: 'Tem certeza que deseja excluir esse comentário?',
        buttons: [
            {
                text: 'Yes',
                role: 'Yes',
                handler: () => {
                    firebase.database().ref('app/feed/'+ this.key[6] + '/comentarios/' + id).update({ status: '0' });
                    

                    firebase.database().ref('app/feed/'+ this.key[6] + '/comentarios').once('value').then(snapshot => {
                        let item = snapshot.val();
                        this.allItems = Object.values(item);
                        this.allIds = Object.keys(item);
                        // console.log(Object.keys(item));
                    });
                }
            },
            {
                text: 'No',
                role: 'No',
                handler: () => {

                    firebase.database().ref('app/feed/' + this.idioma + '/' + this.perfilId + '/' + this.key[6] + '/comentarios').once('value').then(snapshot => {
                        let item = snapshot.val();
                        this.allItems = Object.values(item);
                        this.allIds = Object.keys(item);
                        // console.log(Object.keys(item));
                    });
                }
            }
        ]
    });

    await alert.present();
}
  async erroDelete() {
    const alert = await this.alertController.create({
      header: 'Erro',
      message: 'Você não pode excluir esse comentário',
      buttons: ['Continuar']
    });

    await alert.present();
  }
}
