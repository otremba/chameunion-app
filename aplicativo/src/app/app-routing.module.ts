import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'start', pathMatch: 'full' },
    { path: 'start', loadChildren: './start/start.module#StartPageModule' },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'recover', loadChildren: './recover-password/recover.module#RecoverPageModule' },
    { path: 'cadastro', loadChildren: './cadastro/cadastro.module#CadastroPageModule' },
    { path: 'terms', loadChildren: './terms/terms.module#TermsPageModule' },
    { path: 'pages', loadChildren: './pages/pages.module#PagesPageModule' },
    { path: 'coment-new', loadChildren: './modal/coment-new/coment-new.module#ComentNewPageModule' },
  { path: 'interesse', loadChildren: './modal/interesse/interesse.module#InteressePageModule' },
    

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
