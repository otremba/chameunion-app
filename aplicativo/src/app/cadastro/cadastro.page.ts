//Angular
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

//Ionic
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

//Firebase
import { AngularFireAuth } from '@angular/fire/auth';

//Service Firebase
import { FirebaseService } from '../../service/firebase.service';

import * as firebase from 'firebase';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import AppGlobal from '../app.global';


@Component({
	selector: 'app-cadastro',
	templateUrl: './cadastro.page.html',
	styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

	//var formulario reativo
	public cadastroForm: FormGroup;

	//submit do form
	public submitted: boolean = false;

	public termsCheck: boolean = false;
	public splitEmail:  any;
	public itemFilter:  any;
	public filterItem:  any;


	constructor(
		public formbuilder: FormBuilder,
		public afAuth: AngularFireAuth,
		private navigation: NavController,
		public alertController: AlertController,
		public loading: LoadingController,
		public firebaseErros: FirebaseService,
		public translate: TranslateService,
	) {
		this.cadastroForm = this.formbuilder.group({
			nome: [null, [Validators.required, Validators.minLength(4)]],
			empresa: [null, [Validators.required, Validators.minLength(2)]],
			email: [null, [Validators.required, Validators.email]],
			telefone: [null, [Validators.minLength(8)]],
			password: [null, [Validators.required, Validators.minLength(6)]],
		})
	}

	ngOnInit() {
		
	}

	submitCadastro() {
		this.splitEmail = this.cadastroForm.value.email.split('@', 2);

		console.log(this.splitEmail[1]);

		this.start();
		
		
		if(this.termsCheck) {
			this.submitted = true;

			this.loading.create({ message: this.translate.instant('wait') + "..." }).then(_loading => {
				_loading.present();
			})

			this.afAuth.auth.createUserWithEmailAndPassword(
				this.cadastroForm.value.email, this.cadastroForm.value.password)
				.then(async (response) => {

					let usuario: any = Object.assign({}, this.cadastroForm.value);
					delete usuario.email;
					delete usuario.password;
					usuario.id = response.user.uid;
					if (this.splitEmail[1] == 'chemyunion.com') {
						usuario.perfil_id = 3;	
					}else if(this.splitEmail[1] != 'chemyunion.com'){
						usuario.perfil_id = 4;
					}
					if (this.filterItem.length > 0 ) {
						usuario.tipo = 'Personal Care'
						usuario.perfil_id = 1;
					}else{
						usuario.tipo = ''
						usuario.perfil_id = 4;
					}
					
					usuario.novos_arquivos = 0;
					usuario.created_at = moment().format('YYYY-MM-DD HH:mm:ss');
					usuario.updated_at = usuario.created_at;
					usuario.idioma = this.translate.defaultLang;

					firebase.database().ref('/app/usuarios/' + response.user.uid).set(usuario);
					await AppGlobal.getInstance().setUser(usuario);

					//Mensagem de sucesso ao cadastrar o usuário
					this.showAlert(this.translate.instant('success') + '!', this.translate.instant('register_success') , 'Ok');

					//Enviar o usuário para página home
					this.navigation.navigateRoot('pages/menu', {animated: true});

					//finaliza o loading
					this.loading.dismiss();
				})
				.catch((error) => {


					//verifica se existe o erro no serviço de erros do firebase
					const ErrosFirebase = this.firebaseErros.errosAuth().filter(item => (item.id == error.code));

					//Verifica se existe o erro do firebase
					if (error.code == ErrosFirebase[0].id) {

						//Mostra a mensagem de erro do Firebase
						this.showAlert('', ErrosFirebase[0].name, 'Ok');

						//finaliza o loading
						this.loading.dismiss();
					} else {

						//Mensagem de Erro padrão caso não encontre o erro de firesabe
						this.showAlert('', this.translate.instant('register_error'), 'Voltar');

						//finaliza o loading
						this.loading.dismiss();
					}
				});
			}
			else {
				this.showAlert('', this.translate.instant('terms_check'), 'Ok');
			}
		}

		async showAlert(title: string, msg: string, btn: string) {
			const alert = await this.alertController.create({
				subHeader: title,
				message: msg,
				buttons: [btn]
			});

			await alert.present();
		}

		public openTerms() {
			this.navigation.navigateForward('/terms', {animated: true});
		}

		public onTermChange(event: any) {
			this.termsCheck = event.detail.checked;
		}

		start(){
			firebase.database().ref('app/usuarios-get').once('value').then(snapshot => {
				let user = snapshot.val();
				let allUsers = user;
				this.itemFilter = Object.values(allUsers);
				console.log(this.itemFilter);
				this.documentos(this.cadastroForm.value.email);
			 
			});
		}

		async documentos(email: string){ 
        
			function tipo(tipo) {  
			  return tipo.email == email;
			}
			console.log(this.filterItem = this.itemFilter.filter(tipo));
			console.log(this.filterItem[0]);
			
		  }
	}
