import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThemeModule } from 'src/theme/theme.module';

import { FormulationsPage } from './formulations.page';

const routes: Routes = [
  {
    path: '',
    component: FormulationsPage
  }
];

@NgModule({
  imports: [
    ThemeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FormulationsPage]
})
export class FormulationsPageModule {}
