import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulationsPage } from './formulations.page';

describe('FormulationsPage', () => {
  let component: FormulationsPage;
  let fixture: ComponentFixture<FormulationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulationsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
