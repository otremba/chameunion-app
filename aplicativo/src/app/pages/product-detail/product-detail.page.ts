import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import AppGlobal from 'src/app/app.global';

import * as firebase from 'firebase';
import { FirebaseService } from 'src/service/firebase.service';


import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
    selector: 'app-product-detail',
    templateUrl: './product-detail.page.html',
    styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {

    public loading: boolean;
    public detail: any;
    public title: string;

    constructor(public fbService: FirebaseService,private route: ActivatedRoute, private inappBrowser:InAppBrowser) { }

    ngOnInit() {
        this.loading = true;
        this.title = '';
    }

    public ionViewWillEnter() {
        this.loadData();
    }

    private async loadData() {
        const user: any = await AppGlobal.getInstance().getUser();

        const detailId: string = this.route.snapshot.paramMap.get('id');
        const type: string = this.route.snapshot.paramMap.get('tipo');
        this.title = this.route.snapshot.paramMap.get('titulo');

        let ref = firebase.database().ref('app/produtos/'+ user.idioma + '/' + type).child(detailId);
        ref.once('value').then(snapshot => {
			this.detail = Object.assign({}, snapshot.val());
            this.loading = false;
		});
    }

    onLinkButtonClicked(link){
        const target = '_blank';
        const options = { location : 'no' } ;
        const refLink = this.inappBrowser.create(link,target);
      }
}
