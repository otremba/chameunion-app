import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import AppGlobal from 'src/app/app.global';
import { NavController, AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
    selector: 'app-fav-news',
    templateUrl: './fav-news.page.html',
    styleUrls: ['./fav-news.page.scss'],
})
export class FavNewsPage implements OnInit {

    public loading: boolean;
    public categories: Array<any>;

    constructor(public alertController: AlertController, private navigation: NavController, public firebaseAuth: AngularFireAuth) {
    }

    ngOnInit() {
        this.loading = true;

        this.start();
    }

    public async start() {
        const user: any = await AppGlobal.getInstance().getUser();

        firebase.database().ref('app/noticia_categorias').child(user.idioma).orderByChild('ordem').once('value').then(snapshot => {
			this.categories = [];
			snapshot.forEach(child => {
                let item: any = child.val();

                if(!user.selectNews) {
                    item.selected = true;
                } else {
                    item.selected = (user.selectNews[item.id]) ? true : false;
                }

				this.categories.push(item);
			});
            this.loading = false;
		});
    }

    public selectItem(item: any) {
        item.selected = !item.selected;
    }

    public async save() {
        let selectedItems: any = {};
        for(let item of this.categories) {
            if(item.selected) {
                selectedItems[item.id] = true;
            }
        }

        if(Object.keys(selectedItems).length) {
            firebase.database().ref('app/usuarios/' + this.firebaseAuth.auth.currentUser.uid + '/selectNews').set(selectedItems);
            AppGlobal.getInstance().setUser(Object.assign(await AppGlobal.getInstance().getUser(), {selectNews: selectedItems}));

            this.navigation.back({animated: true});
        }
        else {
            const alert = await this.alertController.create({
    			subHeader: 'Atenção',
    			message: 'Escolha pelo menos 1 categoria',
    			buttons: ['Ok']
    		});
    		await alert.present();
        }
    }
}
