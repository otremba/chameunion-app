import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  constructor(private callNumber: CallNumber) { }

  ngOnInit() {
  }

  callContactBr(){
    this.callNumber.callNumber("+55 15 2102 2000", true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }
  callContactCol(){
    this.callNumber.callNumber("+57 1 302 6555", true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }
  callContactEn(){
    this.callNumber.callNumber("+1  732 529 0964", true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }
  openMapsBR(){
    window.open("maps://maps.apple.com/?q=:"+-23.4523571+","+-47.4181979+"" , "_system"); 
    // window.open("geo:0,0?q=:"+-23.5611372+","+-46.6995142,15+"", "_system");
    // console.log('batata');
    
  }
  openMapsCL(){
    window.open("maps://maps.apple.com/?q=:"+4.6526324+","+-74.3049349+"" , "_system"); 
    // window.open("geo:0,0?q=:"+-23.5611372+","+-46.6995142,15+"", "_system");
    // console.log('batata');
    
  }
  openMapsUS(){
    window.open("maps://maps.apple.com/?q=:"+40.300769+","+-46.6995142,15+"" , "_system"); 
    // window.open("geo:0,0?q=:"+-23.5611372+","+-46.6995142,15+"", "_system");
    // console.log('batata');
    
  }
}
