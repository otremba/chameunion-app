import { Component, ViewChild, ElementRef } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActivatedRoute } from '@angular/router';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { SafeUrl } from '@angular/platform-browser';
import { Platform } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';

import * as firebase from 'firebase';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';

const ZOOM_STEP: number = 0.25;
const DEFAULT_ZOOM: number = 1;

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.page.html',
  styleUrls: ['./post-detail.page.scss'],
})
export class PostDetailPage {

  @ViewChild('playerVideo') playerVideo: ElementRef;
  public videoPlayed: boolean;
  private fileTransfer: FileTransferObject;
  public loading: boolean;
  public downloading: boolean;
  public isLocalFile: boolean;
  public fileItem: any;

  public pdfZoom: number = DEFAULT_ZOOM;
  public srcData: any;

  constructor(private route: ActivatedRoute,
    private socialSharing: SocialSharing,
    private transfer: FileTransfer,
    public platform: Platform,
    private file: File) {

    this.fileTransfer = this.transfer.create();
    this.loading = true;
    this.downloading = false;
    this.isLocalFile = false;
    this.videoPlayed = false;

    this.srcData = {
      url: '',
      withCredentials: false
    }
  }

  public ngAfterViewInit() {
    this.loadData();
  }

  private async loadData() {
    const id: string = this.route.snapshot.paramMap.get('id');

    const fileUrl: string = await AppGlobal.getInstance().getFile(id);

    if (fileUrl) {
      this.srcData.url = this.fixURL(fileUrl);
      this.isLocalFile = true;
    }
    const user: any = await AppGlobal.getInstance().getUser();
    firebase.database().ref('app/feed/').child(id).once('value').then(snapshot => {
      this.fileItem = snapshot.val();
      console.log(this.fileItem);


      // se não existir do storage, seta url do banco
      if (!fileUrl)
        this.srcData.url = this.fileItem.local + this.fileItem.nome;
      else {
        this.file.checkFile(this.file.dataDirectory, id).catch(() => {
          AppGlobal.getInstance().deleteFile(this.fileItem.id);
          this.srcData.url = this.fileItem.local;
          this.isLocalFile = false;
        });
      }

      this.loading = false;
    });
  }

  public setDate(date): Date {
    return moment(date).toDate();
  }

  public play() {
    if (!this.playerVideo.nativeElement.paused) {
      this.videoPlayed = false;
      this.playerVideo.nativeElement.pause();
    }
    else {
      this.videoPlayed = true;
      this.playerVideo.nativeElement.play();
      if (this.platform.is('android')) {
        let fullscreen = this.playerVideo.nativeElement.webkitRequestFullscreen || this.playerVideo.nativeElement.mozRequestFullScreen;
        fullscreen.call(this.playerVideo.nativeElement);
      }
    }
  }

  public download(item: any) {
    this.downloading = true;

    if (!this.isLocalFile) {
      const url = this.fileItem.local + item.nome;

      this.fileTransfer.download(url, this.file.dataDirectory + item.id).then((entry) => {
        AppGlobal.getInstance().setFile(entry.toURL(), item.id);
        this.downloading = false;
        this.isLocalFile = true;
      }, (error) => {
        console.log('error: ' + error);
        this.downloading = false;
      });
    }
    else {
      AppGlobal.getInstance().deleteFile(item.id);
      this.isLocalFile = false;
      this.downloading = false;
    }
  }

  public share(item: any) {
    this.socialSharing.share(item.titulo, item.descricao, null, item.link).then(() => { });
  }

  // pdf
  public zoomIn() {
    this.pdfZoom += ZOOM_STEP;
  }

  public zoomOut() {
    if (this.pdfZoom > DEFAULT_ZOOM) {
      this.pdfZoom -= ZOOM_STEP;
    }
  }

  public resetZoom() {
    this.pdfZoom = DEFAULT_ZOOM;
  }

  // Retornar url Local
  protected fixURL(url: string): string | SafeUrl {
    if (this.platform.is('cordova')) {
      const win: any = window;
      return win.Ionic.WebView.convertFileSrc(url);
    } else {
      return url;
    }
  }
}
