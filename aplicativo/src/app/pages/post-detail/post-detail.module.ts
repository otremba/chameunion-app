import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostDetailPage } from './post-detail.page';
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
  {
    path: '',
    component: PostDetailPage
  }
];

@NgModule({
  imports: [
    ThemeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PostDetailPage]
})
export class PostDetailPageModule {}
