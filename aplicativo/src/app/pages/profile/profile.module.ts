import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePage } from './profile.page';
import { ThemeModule } from '../../../theme/theme.module';

const routes: Routes = [
    {
        path: '',
        component: ProfilePage
    }
];

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [ProfilePage]
})
export class ProfilePageModule {}
