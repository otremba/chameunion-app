import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsPage } from './products.page';
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
    {
        path: '',
        component: ProductsPage
    }
];

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [ProductsPage]
})
export class ProductsPageModule {}
