import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';
import * as firebase from 'firebase/app';
import 'firebase/database';

import * as moment from 'moment';


@Component({
    selector: 'app-products',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

    public products: object;
    public allProducts: any;
    public productList: Array<any>;
    public loading: boolean;
    public fullList: Array<any>;
    public userId: any;
    public filterItem: any;
    public itemFilter: any;
    public userIdioma: any;
    public userEmail: any;
    public queryText: any;
    public list: any;

    

    constructor(private navigation: NavController) {
        this.loading = false;
        this.queryText = '';
    }

    ngOnInit() {
        this.start();
    }

    public async start() {
        this.loading = true;
        const user: any = await AppGlobal.getInstance().getUser();
        this.userEmail = user.tipo;
        this.userIdioma = user.idioma;
        console.log(this.userEmail);
        
         
        firebase.database().ref('app/produtos').child(user.idioma).once('value').then(snapshot => {
            this.products = snapshot.val();
            this.allProducts = this.products;
            this.loading = false;     
            this.userId = user.id;

            console.log(this.allProducts);
            
        });

    }

    public segmentChanged(event: any) {
        if(this.products && this.products[event.detail.value]) {
            let items: Array<any> = [];
            for(const key of Object.keys(this.products[event.detail.value])) {
                const item = Object.assign({}, this.products[event.detail.value][key]);
                item.id = key;
                items.push(item);
            }

            // sort
            items.sort((a, b) => {
                if (a.titulo > b.titulo) {
                    return 1;
                }
                if (a.titulo < b.titulo) {
                    return -1;
                }
                return 0;
            });

            this.productList = items;
            this.fullList = items;
            
        }
    }

    public openDetail(item: any) {
        this.navigation.navigateForward('/pages/menu/docs/products/' + item.tipo + '/' + item.id + '/' + item.titulo, {animated: true});
        
        let date = moment().format('YYYY-MM-DD HH:mm:ss');
        firebase.database().ref('/app/logs/' + this.userId +'/product/' + item.titulo).set(date);
    }   

    setFilteredItems(searchTerm: any){
        let val = searchTerm.target.value;
        if (val && val.trim() != ""){
            this.productList = this.fullList;
            this.productList = this.productList.filter((produto) => {
                return(produto.tags.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }else{
            this.productList = this.fullList;
        }
    }  
    
}


