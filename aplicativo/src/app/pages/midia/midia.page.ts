import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import AppGlobal from 'src/app/app.global';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { NavController } from '@ionic/angular';

import { File } from "@ionic-native/file/ngx";
import {
  FileTransfer,
  FileTransferObject
} from "@ionic-native/file-transfer/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";

@Component({
    selector: 'app-midia',
    templateUrl: './midia.page.html',
    styleUrls: ['./midia.page.scss'],
})
export class MidiaPage implements OnInit {

    public loading: boolean;
    public categories: Array<object>;
    public categories1: Array<object>;
    public allItems: any;
    public PreviewAnyFile: any;
    public filterItem: any;
    public filterItem1: any;
	  public userId: any;


    constructor(public firebaseAuth: AngularFireAuth, private navigation: NavController, private fileOpener: FileOpener,
        private transfer: FileTransfer,
        private file: File) {

        this.loading = false;
        this.categories = [];
        this.categories1 = [];
    }

    ngOnInit() {
        this.loading = true;
        this.start();
    }

    public async start() {
        const user: any = await AppGlobal.getInstance().getUser();
        this.userId = user.id;
        firebase.database().ref('app/docs/midia').child(user.idioma).once('value').then(snapshot => {
                let item;
                // : Object = child.val();
                item = snapshot.val();
                this.allItems = Object.values(item);
                console.log(this.allItems);

                if(user.idioma == 'pt'){
                  this.categories.push({
                      nome: 'Documentos Legais',
                    });
                  this.categories1.push(
                  {
                    nome: 'Certificados'
                  });
                }
                if(user.idioma == 'en'){
                  this.categories.push({
                      nome: 'RRMI',
                    });
                  this.categories1.push(
                  {
                    nome: 'Certification'
                  });
                }
                
                item['open'] = false;
            this.loading = false;
        });
    }

    async documentos(){
        function tipo(tipo) { 
          return tipo.tipo == 'Documentos Legais';
        }
        console.log(this.filterItem = this.allItems.filter(tipo));
        this.loading = false;
    }
    async certificados(){
        function tipo(tipo) { 
          return tipo.tipo == 'Certificados';
        }
        console.log(this.filterItem1 = this.allItems.filter(tipo));
        this.loading = false;
    }
    async rrmi(){
      function tipo(tipo) { 
        return tipo.tipo == 'RRMI';
      }
      console.log(this.filterItem = this.allItems.filter(tipo));
      this.loading = false;
    }
    async certificates(){
        function tipo(tipo) { 
          return tipo.tipo == 'Certificados';
        }
        console.log(this.filterItem1 = this.allItems.filter(tipo));
        this.loading = false;
    }

    public openCategory(category: object) {
        category['open'] = !category['open'];
        // console.log(category['nome']);
        
        if (category['nome'] == 'Documentos Legais' ) {
          this.documentos();  
        }if(category['nome'] == 'Certificados'){
          this.certificados();
        }if(category['nome'] == 'Certification'){
          this.certificates();
        }if(category['nome'] == 'RRMI'){
          this.rrmi();
        }
        
        console.log(category);
        
    }

    public openDetail(id: string) {
        this.navigation.navigateForward('/pages/menu/docs/media/' + id, {animated: true});

        let date = moment().format('YYYY-MM-DD HH:mm:ss');
			  firebase.database().ref('/app/logs/' + this.userId +'/documentos/qualidade/' + id).set(date);
    }

    public setDate(date): Date {
		return moment(date).toDate();
    }
    download(url: string, title: string) {
    this.PreviewAnyFile = this.transfer.create();
    this.PreviewAnyFile
      .download(url, this.file.dataDirectory + title + ".pdf")
      .then(entry => {
        console.log("download complete: " + entry.toURL());
        this.fileOpener
          .open(entry.toURL(), "application/pdf")
          .then(() => console.log("File is opened"))
          .catch(e => console.log("Error opening file", e));
      });
  }
}
