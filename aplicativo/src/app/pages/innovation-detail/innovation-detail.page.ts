import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common' 
import * as firebase from 'firebase/app';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';


@Component({
  selector: 'app-innovation-detail',
  templateUrl: './innovation-detail.page.html',
  styleUrls: ['./innovation-detail.page.scss'],
})
export class InnovationDetailPage implements OnInit {
  public text: string;
  public id: any;
  public products: any;
  private fileTransfer: FileTransferObject;


  constructor(location: Location,
  private transfer: FileTransfer,
  private file: File) {
  this.text = '<p>Carregando...</p>';
  this.fileTransfer = this.transfer.create();
  let url = location.path();
  this.id = url.split('/')[5]
  console.log(this.id);
    
  }

  ngOnInit() {
    this.start();
  }

  async start(){
    firebase.database().ref('app/open-innovation/' + this.id).once('value').then(snapshot => {
      this.products = snapshot.val();
      console.clear();
    });
  }

  download() {
    const url = this.products.archive;
    this.fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
      console.log('download complete: ' + entry.toURL());
    }, (error) => {
    });
  }
}
