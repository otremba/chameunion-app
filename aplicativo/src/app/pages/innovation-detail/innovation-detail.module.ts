import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InnovationDetailPage } from './innovation-detail.page';
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
  {
    path: '',
    component: InnovationDetailPage
  }
];

@NgModule({
  imports: [
    ThemeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InnovationDetailPage]
})
export class InnovationDetailPageModule {}
