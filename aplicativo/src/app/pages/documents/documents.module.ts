import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentsPage } from './documents.page';
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
  {
    path: '',
    component: DocumentsPage
  }
];

@NgModule({
  imports: [
    ThemeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DocumentsPage]
})
export class DocumentsPageModule {}
