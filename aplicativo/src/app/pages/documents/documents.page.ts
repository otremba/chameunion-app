import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import AppGlobal from 'src/app/app.global';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.page.html',
  styleUrls: ['./documents.page.scss'],
})
export class DocumentsPage implements OnInit {
  public loading: boolean;
  public categories: Array<object>;
  public categories1: Array<object>;
  public categories2: Array<object>;
  public allItems: any;
  public filterItem: any;
  public filterItem1: any;
  public filterItem2: any;
	public userId: any;





  constructor(public firebaseAuth: AngularFireAuth, private navigation: NavController) {

    this.loading = false;
    this.categories = [];
    this.categories1 = [];
    this.categories2 = [];
   }
  
  ngOnInit() {
    this.loading = true;
    this.start();
  }

    public async start() {
      const user: any = await AppGlobal.getInstance().getUser();
      this.userId = user.id;

      firebase.database().ref('app/docs/midia').child(user.idioma).once('value').then(snapshot => {
        let item = snapshot.val();
        this.allItems = Object.values(item);
               
        
    });
      this.categories.push({
        nome: 'Agen',
      });
      this.categories1.push(
        {
          nome: 'Sensoveil Soft'
        });
      this.categories2.push(
        {
          nome: 'ThermoShield Premium'
        });
    }

    filter(item){
      switch (item) {
        default:
          break;
      }
    }

    async agen(){
      function tipo(tipo) { 
        return tipo.tipo == 'Agen';
      }


      // this.filter1 = this.allItems.filter(tipo);
      console.log(this.filterItem = this.allItems.filter(tipo));

      // console.log(this.allItems);
      
      // this.allItems['open'] = false; 
      this.loading = false;
    }
    async sensoveil(){
      function tipo(tipo) { 
        return tipo.tipo == 'Sensoveil Soft';
      }

      // this.filter2 = this.allItems.filter(tipo);
      console.log(this.filterItem1 = this.allItems.filter(tipo));
      
      // this.allItems['open'] = false;                
        
      this.loading = false;
    }
    async premium(){
      function tipo(tipo) { 
        return tipo.tipo == 'ThermoShield Premium';
      }

      // this.filter3 = this.allItems.filter(tipo);
      console.log(this.filterItem2 = this.allItems.filter(tipo));
      
      // this.allItems['open'] = false;                
      this.loading = false;
    }

    public openCategory(category: object) {
        category['open'] = !category['open'];
        // console.log(category['nome']);
        
        if (category['nome'] == 'Agen' ) {
          this.agen();  
        }else if(category['nome'] == 'Sensoveil Soft'){
          this.sensoveil();
        }else{
          this.premium();
        }
        
        console.log(category);
        
    }

    public openDetail(id: string) {
        this.navigation.navigateForward('/pages/menu/docs/media/' + id, {animated: true});


        let date = moment().format('YYYY-MM-DD HH:mm:ss');
			  firebase.database().ref('/app/logs/' + this.userId +'/documentos/lançamentos/' + id).set(date);
        
    }

    public setDate(date): Date {
		return moment(date).toDate();
	}
}

