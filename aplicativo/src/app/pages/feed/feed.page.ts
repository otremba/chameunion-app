import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import * as firebase from 'firebase';
import { FirebaseService } from 'src/service/firebase.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';
import { ModalController } from '@ionic/angular';
import { ComentPostPage } from '../../modal/coment-post/coment-post.page';
import { PostModal } from '../../modal/post-modal/post-modal.page';



@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage implements OnInit {
  private dbRef: firebase.database.Reference
  public userId: any;
  public allItems: any;
  public products: any;


  constructor(
    public fbService: FirebaseService,
    public firebaseAuth: AngularFireAuth,
    private navigation: NavController,
    public modalController: ModalController,

  ) {

  }

  ngOnInit() {
    this.start();
  }

  public async start() {
    const user: any = await AppGlobal.getInstance().getUser();
    firebase.database().ref('app/feed').once('value').then(snapshot => {
      let item = snapshot.val();
      this.allItems = Object.values(item);
      console.log(this.allItems);
      console.log(this.products);

      this.allItems = this.allItems.slice(Math.max(this.allItems.length - 7, 0)).reverse();

    });

    this.userId = user.id;

  }

  async doRefresh(event) {
    const user: any = await AppGlobal.getInstance().getUser();
    firebase.database().ref('app/feed').once('value').then(snapshot => {
      let item = snapshot.val();
      this.allItems = Object.values(item);
      console.log(this.allItems);
      console.log(this.products);
      this.allItems = this.allItems.slice(Math.max(this.allItems.length - 7, 1)).reverse();

    });
    this.userId = user.id;

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  public addLike(item: any) {
    this.fbService.incrementItem('likes', item, this.dbRef.child(item.id), this.firebaseAuth.auth.currentUser.uid);
    // firebase.database().ref('app/feed/' + products.id).child("likes").set({
      // id: true 
    // });
    console.log(item);
    
  }

  async presentModal(post) {
    this.navigation.navigateForward('/pages/menu/feed/' + post.id, { animated: true });
    console.log(post);
    
  }
  async postModal() {
    const modal = await this.modalController.create({
      component: PostModal,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }
  public openDetail(id: string) {
    console.log(id);
    
    this.navigation.navigateForward('/pages/menu/docs/post-detail/' + id, {animated: true});

    // let date = moment().format('YYYY-MM-DD HH:mm:ss');
    // firebase.database().ref('/app/logs/' + this.userId +'/documentos/formula/' + id).set(date);
  }
}
