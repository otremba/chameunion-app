import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as moment from 'moment';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import AppGlobal from '../../app.global';

import * as firebase from 'firebase';
import { NavController } from '@ionic/angular';
import { FirebaseService } from 'src/service/firebase.service';

import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-news',
	templateUrl: './news.page.html',
	styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {

	public categories: Array<object>;
	public categorySelected: string;
	private dbRef: firebase.database.Reference;
	public loading: boolean;
	public filterItem: any;
	public idFilter: any;
	public userId: any;

	public news: Array<object> = [];

	public slideOpts = {
		slidesPerView: 2.2,
		spaceBetween: 8,
	};

	constructor(public firebaseAuth: AngularFireAuth,
		public fbService: FirebaseService,
		private socialSharing: SocialSharing,
		private navigation: NavController,
		private activatedRoute: ActivatedRoute
	) {
		this.loading = true;
	}

	ngOnInit() {
		this.start();
	}

	public ionViewDidEnter() {
		if (this.categories && this.categories.length && this.categorySelected) {
			this.selectCategory(this.categorySelected);
		}
		if (this.categorySelected == null) {
			setTimeout(() => {
				document.getElementById('-LaqNEHy94Ql6XL8Q4gw').click();
			}, 2000);
		}
		
	}

	public async start() {
		const user: any = await AppGlobal.getInstance().getUser();

		this.userId = user.id;
		firebase.database().ref('app/noticia_categorias').child(user.idioma).orderByChild('ordem').once('value').then(snapshot => {
			this.categories = [];
			snapshot.forEach(child => {
				if (!user.selectNews || user.selectNewxs[child.key])
					this.categories.push(child.val());
			});

			if (this.categories.length > 0) {
				this.loading = false;
				this.selectCategory(this.categories[0]['id']);
 
			}
		});
	}

	public openDetail(item: any) {
		this.navigation.navigateForward('/pages/menu/news/' + item.id, { animated: true });

		let date = moment().format('YYYY-MM-DD HH:mm:ss');
			firebase.database().ref('/app/logs/' + this.userId +'/news/' + item.id).set(date);
	}

	public async selectCategory(id: string) {
		function categoria(categoria) {
			// console.log(this.idFilter);
			return categoria.categoria == id;

		}
		console.log(this.filterItem = this.news.filter(categoria));
		if (!this.loading) {
			if (this.categorySelected !== id) {
				this.loading = true;
				this.news = [];
			}

			this.categorySelected = id;


			const user: any = await AppGlobal.getInstance().getUser();
			this.dbRef = firebase.database().ref('app/noticias').child(user.idioma).child(user.perfil_id);
			// console.log(this.dbRef);

			this.dbRef.orderByChild('de').once('value').then(snapshot => {
				this.news = [];

				snapshot.forEach(child => {
					this.news.unshift(Object.assign({}, child.val()));
				});
				this.loading = false;
			});

		}


	}

	public addLike(item: any) {
		
		this.fbService.incrementItem('likes', item, this.dbRef.child(item.id), this.firebaseAuth.auth.currentUser.uid);
		
	}

	public addShare(item: any) {
		this.socialSharing.share(item.titulo, item.titulo, null, item.link).then(() => {
			this.fbService.incrementItem('share', item, this.dbRef.child(item.id), String(new Date().getTime()));
		});
	}
	public setDate(date): Date {
		return moment(date).toDate();
	}
}
