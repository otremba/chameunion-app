import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThemeModule } from 'src/theme/theme.module';

import { MyOrdersPage } from './my-orders.page';

const routes: Routes = [
  {
    path: '',
    component: MyOrdersPage
  }
];

@NgModule({
  imports: [
      ThemeModule,
      RouterModule.forChild(routes)
  ],
  declarations: [MyOrdersPage]
})
export class MyOrdersPageModule {}
