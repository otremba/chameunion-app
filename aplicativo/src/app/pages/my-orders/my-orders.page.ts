import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';

import * as firebase from 'firebase/app';
import 'firebase/database';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.page.html',
  styleUrls: ['./my-orders.page.scss'],
})
export class MyOrdersPage implements OnInit {

  public products: object;
  public fullList: any;
  public productList: Array<any>;
  public userId: any;
  public allItems: any;

  constructor(private navigation: NavController) {
    

  }

  ngOnInit() {
    this.start()
}
public openOrder(){
  this.navigation.navigateForward('/pages/menu/order-detail', {animated: true});
}

public async start() {
  const user: any = await AppGlobal.getInstance().getUser();
  this.userId = user.id;
  firebase.database().ref('app/orders').once('value').then(snapshot => {
          let item;
          item = snapshot.val();
          this.allItems = Object.values(item);
          console.log(this.allItems);
          
          item['open'] = false;
      // this.loading = false;
  });
}

  public segmentChanged(event: any) {
    if(this.products && this.products[event.detail.value]) {
        let items: Array<any> = [];
        for(const key of Object.keys(this.products[event.detail.value])) {
            const item = Object.assign({}, this.products[event.detail.value][key]);
            item.id = key;
            items.push(item);
        }

        // sort
        items.sort((a, b) => {
            if (a.title > b.title) {
                return 1;
            }
            if (a.title < b.title) {
                return -1;
            }
            return 0;
        });

        this.productList = items;
        this.fullList = items;
        
    }
  }

}
