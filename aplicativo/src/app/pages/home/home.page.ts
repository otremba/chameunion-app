import { Component, ViewChild } from "@angular/core";
import { Slides } from "ionic-angular";
import AppGlobal from "src/app/app.global";
import * as firebase from "firebase";
import * as moment from "moment";
import { NavController, PopoverController } from "@ionic/angular";
import { FirebaseService } from "src/service/firebase.service";

import { NotificationsComponent } from "./../../components/notifications/notifications.component";
import { Firebase } from "@ionic-native/firebase/ngx";
// import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  @ViewChild(Slides) slides: Slides;

  public products: object;
  public allProducts: any;
  public highlights: Array<any>;
  public lastNews: Array<any>;
  public fullNews: Array<any>;
  public lastNewsData: Array<any>;
  public userId: any;
  public notices: any;
  public notice: any;
  public userIdioma: any;

  public filterHighlights: any;

  sliderConfig = {
    slidesPerView: 1.6,
    spaceBetween: 10,
    centeredSlides: false,
  };

  sliderConfiOpen = {
    slidesPerView: 2,
    spaceBetween: 10,
    centeredSlides: false,
  };

  public tvCorporativa = {
    title: "TV CORPORATIVA",
    text:
      "Retenção de água no interior dos fios é a chave para proteger os cabelos",
    data: "7 SET",
    view: "350",
    amei: "25",
    img: "pgina-inicial-com-rolagem-rectangle-copy.jpg",
  };

  constructor(
    public fbService: FirebaseService,
    private navigation: NavController,
    public popoverCtrl: PopoverController,
    private firebase: Firebase
  ) {
    this.firebase.setScreenName("HomeIonic");
  }

  ngOnInit() {
    this.start();
  }

  public async start() {
    const user: any = await AppGlobal.getInstance().getUser();

    this.userIdioma = user.idioma;

    firebase
      .database()
      .ref("app/destaques")
      .child(user.idioma)
      .orderByChild("created_at")
      .once("value")
      .then((snapshot) => {
        this.highlights = [];
        snapshot.forEach((child) => {
          this.highlights.push(child.val());
          this.userId = user.id;
        });
      });

    firebase
      .database()
      .ref("app/noticias")
      .child(user.idioma)
      .child(user.perfil_id)
      .orderByChild("created_at")
      .once("value")
      .then((snapshot) => {
        this.products = snapshot.val();

        this.allProducts = Object.values(this.products);

        // this.loading = false;
        // this.userId = user.id;

        this.notices = this.allProducts;

        function categoria2(categoria) {
          if (user.idioma == "pt") {
            return categoria.categoria != "-LaqNEHy94Ql6XL8Q5fs";
          } else {
            return categoria.categoria != "-Lb8g5It1AcMe8xzxlb9";
          }
        }

        console.log((this.notice = this.notices.filter(categoria2)));
        this.notice.sort(function (a, b) {
          return Math.random() - 0.5;
        });
        this.notice = this.notice
          .slice(Math.max(this.notice.length - 7, 1))
          .reverse();

        function categoria(categoria) {
          if (user.idioma == "pt") {
            return categoria.categoria == "-LaqNEHy94Ql6XL8Q5fs";
          } else {
            return categoria.categoria == "-Lb8g5It1AcMe8xzxlb9";
          }
        }
        console.log(
          (this.filterHighlights = this.allProducts.filter(categoria).reverse())
        );
      });
  }

  public setDate(date): Date {
    return moment(date).toDate();
  }

  public openHighlight(news) {
    let stringExemplo = news.id;
    let resultado = stringExemplo.split("/", 7);
    console.log(resultado);

    this.navigation.navigateForward("/pages/menu/news/" + resultado[0], {
      animated: true,
    });
  }

  public openNews(news) {
    if (news.id)
      this.navigation.navigateForward("/pages/menu/news/" + news.id, {
        animated: true,
      });
    let date = moment().format("YYYY-MM-DD HH:mm:ss");
    firebase
      .database()
      .ref("/app/logs/" + this.userId + "/news/" + news.id)
      .set(date);
  }

  public openProducts() {
    this.navigation.navigateForward("/pages/menu/docs/products", {
      animated: true,
    });
  }

  // setFilteredItems(searchTerm: any){
  // 	let val = searchTerm.target.value;
  // 	function sortfunction(a, b){
  // 		return (a - b) //faz com que o array seja ordenado numericamente e de ordem crescente.
  // 	}
  // 	console.log(this.lastNews);
  //     if (val && val.trim() != ""){
  //         this.lastNews = this.lastNews.filter((noticias) => {

  // 			return(noticias.conteudo.toLowerCase().indexOf(val.toLowerCase()) > -1);
  //         })
  //     }else{
  //         this.lastNews = this.fullNews;

  //     }
  // }

  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true,
    });
    return await popover.present();
  }

  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 2000,
    autoplay: true,
  };
}
