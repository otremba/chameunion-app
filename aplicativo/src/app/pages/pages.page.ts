import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { Router, RouterEvent } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import AppGlobal from 'src/app/app.global';

//Storage
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import * as firebase from 'firebase/app';

@Component({
    selector: 'pages-router',
    templateUrl: './pages.page.html',
    styleUrls: ['./pages.page.scss'],
})
export class PagesPage {

    public selectPath: string;
    public pages: Array<Object>;
    public list: any;
    public selectPage: any;
    public menuInteract: boolean;
    public userType: any;
    public userIdioma: any;
    

    constructor(
        private menu: MenuController,
        private navigation: NavController,
        private router: Router,
        private firebaseAuth: AngularFireAuth,
        private storage: Storage
    ) {
        this.menuInteract = false;
        this.selectPath = '/pages/menu/home';

        
        

        this.router.events.subscribe((event: RouterEvent) => {
            this.selectPath = event.url;
        });
    }
    ngOnInit() {
        this.start();
      }

    async start(){
        const user: any = await AppGlobal.getInstance().getUser();

        this.userIdioma = user.idioma;

        firebase.database().ref('app/usuarios/' + user.id).once('value').then(snapshot => {
            this.userType = snapshot.val();

            if (this.userIdioma == 'pt') {
                if (this.userType.perfil_id == 4) {
                    this.pages = [
                        {
                            icon: 'home',
                            url: '/pages/menu/home',
                            title: 'menu.home'
                        },
                        {
                            image: '/assets/images/icon/submenu-products.png',
                            url: '/pages/menu/docs/products',
                            title: 'menu.products',
                            
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-noticias.png',
                            url: '/pages/menu/news',
                            title: 'menu.news'
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-noticias.png',
                            url: '/pages/menu/feed',
                            title: 'menu.feed'
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-my-community.png',
                            url: '/pages/menu/account',
                            title: 'menu.profile',
                        },
                        {
                            image: '/assets/images/icon/menu-orders.png',
                            url: '/pages/menu/my-orders',
                            title: 'menu.my-orders',
                        },
                        // {
                        //     image: '/assets/images/icon/menu-innovations.png',
                        //     url: '/pages/menu/innovations',
                        //     title: 'menu.innovations',
                        // },
                        // {
                        //     image: '/assets/images/icon/menu-colaborador-docs.png',
                        //     title: 'menu.doc',
                        //     url: '/pages/menu/docs/media',
                        // },
                        {
                            image: '/assets/images/icon/menu-sobre.png',
                            url: '/pages/menu/about',
                            title: 'menu.about'
                        },
                        {
                            image: '/assets/images/icon/contato.png',
                            url: '/pages/menu/contact',
                            title: 'menu.contact'
                        }
                    ];
                }else{
                    this.pages = [
                        {
                            icon: 'home',
                            url: '/pages/menu/home',
                            title: 'menu.home'
                        },
                        {
                            image: '/assets/images/icon/submenu-products.png',
                            url: '/pages/menu/docs/products',
                            title: 'menu.products',
                            
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-noticias.png',
                            url: '/pages/menu/news',
                            title: 'menu.news'
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-noticias.png',
                            url: '/pages/menu/feed',
                            title: 'menu.feed'
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-my-community.png',
                            url: '/pages/menu/account',
                            title: 'menu.profile',
                        },
                        {
                            image: '/assets/images/icon/menu-orders.png',
                            url: '/pages/menu/my-orders',
                            title: 'menu.my-orders',
                        },
                        // {
                        //     image: '/assets/images/icon/menu-innovations.png',
                        //     url: '/pages/menu/innovations',
                        //     title: 'menu.innovations',
                        // },
                        // {
                        //     image: '/assets/images/icon/menu-colaborador-docs.png',
                        //     title: 'menu.formulation',
                        //     url: '/pages/menu/formulations',
                        // },
                        // {
                        //     image: '/assets/images/icon/menu-colaborador-docs.png',
                        //     title: 'menu.doc',
                        //     url: '/pages/menu/docs/media',
                        // },
                        // {
                        //     image: '/assets/images/icon/menu-colaborador-docs.png',
                        //     title: 'menu.documents',
                        //     url: '/pages/menu/documents',
                        // },
            
                        {
                            image: '/assets/images/icon/menu-sobre.png',
                            url: '/pages/menu/about',
                            title: 'menu.about'
                        },
                        {
                            image: '/assets/images/icon/contato.png',
                            url: '/pages/menu/contact',
                            title: 'menu.contact'
                        }
                    ];
                }
            }else{
                if (this.userType.perfil_id == 4) {
                    this.pages = [
                        {
                            icon: 'home',
                            url: '/pages/menu/home',
                            title: 'menu.home'
                        },
                        {
                            image: '/assets/images/icon/submenu-products.png',
                            url: '/pages/menu/docs/products',
                            title: 'menu.products',
                            
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-noticias.png',
                            url: '/pages/menu/news',
                            title: 'menu.news'
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-noticias.png',
                            url: '/pages/menu/feed',
                            title: 'menu.feed'
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-my-community.png',
                            url: '/pages/menu/account',
                            title: 'menu.profile',
                        },
                        {
                            image: '/assets/images/icon/menu-orders.png',
                            url: '/pages/menu/my-orders',
                            title: 'menu.my-orders',
                        },
                        // {
                        //     image: '/assets/images/icon/menu-innovations.png',
                        //     url: '/pages/menu/innovations',
                        //     title: 'menu.innovations',
                        // },
                        // {
                        //     image: '/assets/images/icon/menu-colaborador-docs.png',
                        //     title: 'menu.doc',
                        //     url: '/pages/menu/docs/media',
                        // },
                        {
                            image: '/assets/images/icon/menu-sobre.png',
                            url: '/pages/menu/about',
                            title: 'menu.about'
                        },
                        {
                            image: '/assets/images/icon/contato.png',
                            url: '/pages/menu/contact',
                            title: 'menu.contact'
                        }
                    ];
                }else{
                    this.pages = [
                        {
                            icon: 'home',
                            url: '/pages/menu/home',
                            title: 'menu.home'
                        },
                        {
                            image: '/assets/images/icon/submenu-products.png',
                            url: '/pages/menu/docs/products',
                            title: 'menu.products',
                            
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-noticias.png',
                            url: '/pages/menu/news',
                            title: 'menu.news'
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-noticias.png',
                            url: '/pages/menu/feed',
                            title: 'menu.feed'
                        },
                        {
                            image: '/assets/images/icon/menu-colaborador-my-community.png',
                            url: '/pages/menu/account',
                            title: 'menu.profile',
                        },
                        {
                            image: '/assets/images/icon/menu-orders.png',
                            url: '/pages/menu/my-orders',
                            title: 'menu.my-orders',
                        },
                        // {
                        //     image: '/assets/images/icon/menu-innovations.png',
                        //     url: '/pages/menu/innovations',
                        //     title: 'menu.innovations',
                        // },
                        // {
                        //     image: '/assets/images/icon/menu-colaborador-docs.png',
                        //     title: 'menu.formulation',
                        //     url: '/pages/menu/formulations',
                        // },
                        // {
                        //     image: '/assets/images/icon/menu-colaborador-docs.png',
                        //     title: 'menu.doc',
                        //     url: '/pages/menu/docs/media',
                        // },
                        // {
                        //     image: '/assets/images/icon/menu-colaborador-docs.png',
                        //     title: 'menu.documents',
                        //     url: '/pages/menu/documents',
                        // },
            
                        {
                            image: '/assets/images/icon/menu-sobre.png',
                            url: '/pages/menu/about',
                            title: 'menu.about'
                        },
                        {
                            image: '/assets/images/icon/contato.png',
                            url: '/pages/menu/contact',
                            title: 'menu.contact'
                        }
                    ];
                }
            }
            
        
        });
    }

    public onMenuClosed() {
        this.menuInteract = false;
    }

    public closeMenu() {
        this.menu.close();
    }

    public logout() {
        this.firebaseAuth.auth.signOut();
        this.storage.set('user', null);
        this.navigation.navigateRoot('/start');
    }

    public openPage(page: any, parent?: any) {

        if(this.selectPage && this.selectPage.subPages && this.selectPage != page && !parent)
            this.selectPage.active = false;

        this.selectPage = page;

        if(page.url) {
            this.closeMenu();
            this.navigation.navigateRoot(page.url, {animated: true});
            for(let pageItem of this.pages) {
                if(pageItem['subPages'] && pageItem != parent)
                    pageItem['active'] = false;
            }
        }
        else {
            page.active = !page.active;
            this.menuInteract = true;
        }
    }
}
