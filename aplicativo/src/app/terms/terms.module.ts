import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TermsPage } from './terms.page';
import { ThemeModule } from '../../theme/theme.module';

const routes: Routes = [
    {
        path: '',
        component: TermsPage
    }
];

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [TermsPage]
})
export class TermsPageModule {}
