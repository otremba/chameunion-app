import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from '../components/components.module';
import { SafePipe } from '../pipes/SafePipe';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FirebaseService } from 'src/service/firebase.service';
import { TranslateModule } from '@ngx-translate/core';


//
const BASE_MODULES = [
	CommonModule,
	FormsModule,
	IonicModule,
	ComponentsModule,
	ReactiveFormsModule,
	PdfViewerModule,
	TranslateModule
]

const PIPES = [
	SafePipe
]

const PROVIDERS = [
	SocialSharing,
	FirebaseService
]

@NgModule({
	imports: [
		...BASE_MODULES,
	],
	exports: [
		...BASE_MODULES, ...PIPES
	],
	declarations: [
		...PIPES
	],
	providers: [
		...PROVIDERS
	]
})
export class ThemeModule {}
