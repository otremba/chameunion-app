import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BtnPadraoComponent } from './btn-padrao/btn-padrao';
import { HeaderComponent } from './header/header';

const Components = [
	BtnPadraoComponent,
	HeaderComponent
]

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		...Components,
	],
	exports: [
		...Components,
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule { }
